function prototype(element) {
	var result = element.clone();
	result.removeAttr('data-prototype');
	result.removeAttr('id');
	
	return result;
}

function showContent() {
	$('[data-prototype="true"]').hide();
	$('[data-role="content"]').show();
}
