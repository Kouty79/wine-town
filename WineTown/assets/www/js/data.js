var Locations = {
	PalazzoPitti : {
		label : "Palazzo Pitti",
		coords : {
			lat : 43.765182,
			long : 11.250273
		}
	},

	PalazzoAntinori : {
		label : "Palazzo Antinori",
		coords : {
			lat : 43.772769,
			long : 11.251313
		}
	},

	PalazzoDavanzati : {
		label : "Palazzo Davanzati",
		coords : {
			lat : 43.770245,
			long : 11.252843
		}
	}

};

var Cantine = {
	// Pitti
	Bolsignano : {
		label : "Bolsignano",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	FattoriaDelPino : {
		label : "Fattoria del Pino",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : [ "Rosso di Montalcino 2010", "IGT Vinval� 2010", "IGT Vinval� 2011" ]
	},
	FattoriaDeiBarbi : {
		label : "Fattoria dei Barbi",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	IlMarroneto : {
		label : "Il Marroneto",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	LeRagnaie : {
		label : "Le Ragnaie",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	LeChiuse : {
		label : "Le Chiuse",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	Piombaia : {
		label : "Piombaia",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	Pietroso : {
		label : "Pietroso",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	CorteDeiVenti : {
		label : "Corte dei Venti",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	Grazia : {
		label : "Grazia",
		origin : "Montalcino",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	Ventolaio : {
		label : "Ventolaio",
		origin : "Montalcino - Poggio d'Arna",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	CappellaDiSantAndrea : {
		label : "Cappella di Sant'Andrea",
		origin : "San Gimignano",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	Cigliano : {
		label : "Cigliano",
		origin : "San Casciano Val di Pesa",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	FattoriaDiCinciano : {
		label : "Fattoria di Cinciano",
		origin : "Val d'Elsa",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	CastelloDiMonsanto : {
		label : "Castello di Monsanto",
		origin : "Val d'Elsa",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	LeCinciole : {
		label : "Le Cinciole",
		origin : "Panzano",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	FattoriaVignavecchia : {
		label : "Fattoria Vignavecchia",
		origin : "Radda in Chianti",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	ErtadiRadda : {
		label : "L'erta di Radda",
		origin : "Radda in Chianti",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	Montemaggio : {
		label : "Montemaggio",
		origin : "Radda in Chianti",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	Montevertine : {
		label : "Montevertine",
		origin : "Radda in Chianti",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	ValDelleCorti : {
		label : "Val delle Corti",
		origin : "Radda in Chianti",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	BadiaAColtibuono : {
		label : "Badia a Coltibuono",
		origin : "Monti in Chianti - Gaiole",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	PalazzoDiPiero : {
		label : "Palazzo di Piero",
		origin : "Sarteano",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	CastelloDiPotentino : {
		label : "Castello di Potentino",
		origin : "Seggiano - Amiata",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	TenutaCasteani : {
		label : "Tenuta Casteani",
		origin : "Gavorrano",
		location : Locations.PalazzoPitti,
		etichette : []
	},
	CostaArchi : {
		label : "Costa Archi",
		origin : "Romagna",
		location : Locations.PalazzoPitti,
		etichette : []
	},

	// Antinori
	MarchesiAntinori : {
		label : "Marchesi Antinori",
		origin : "",
		location : Locations.PalazzoAntinori,
		etichette : []
	},

	// Davanzati
	IlBorro : {
		label : "Il Borro",
		origin : "",
		location : Locations.PalazzoDavanzati,
		etichette : []
	}
}
