function MapsController(map) {
	this.map = map;
}

MapsController.prototype.showCurrentPosition = function(callback) {
	var controller = this;

	navigator.geolocation.getCurrentPosition(function(position) {
		var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		var marker = new google.maps.Marker({
			position : myLatlng,
			title : "Tu sei qui"
		});

		marker.setMap(controller.map);

		if (callback != null) {
			callback();
		}

	}, function() {
		alert('Impossibile determinare la tua posizione.');
	});
};

function TestMapsController(map) {
	this.map = map;
}

TestMapsController.prototype.showCurrentPosition = function(callback) {
	var controller = this;

	setTimeout(function() {
		var myLatlng = new google.maps.LatLng(43.768035, 11.253169);
		var marker = new google.maps.Marker({
			position : myLatlng,
			title : "Tu sei qui"
		});

		marker.setMap(controller.map);
		
		if (callback != null) {
			callback();
		}
	}, 1000);
};
