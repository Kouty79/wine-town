function CantineController(view) {
	this.view = view;
}

CantineController.prototype.loadCantine = function() {
	var controller = this;

	var cantine = getCantineArray(Cantine);
	sort(cantine);
	$.each(cantine, function(index, cantina) {
		addCantinaToView(cantina);
	});
	controller.view.refresh();

	function getCantineArray(cantine) {
		var elements = [];
		$.each(cantine, function(key, value) {
			elements.push(value);
		});

		return elements;
	}

	function sort(elements) {
		elements.sort(function(aObj, bObj) {
			var a = aObj.label;
			var b = bObj.label;
			return ((a < b) ? -1 : ((a > b) ? 1 : 0));
		});
	}

	function addCantinaToView(cantina) {
		var entry = controller.view.createEntry(cantina.label);
		entry.click(createCantinaClickHandler(cantina));
		controller.view.add(entry);
	}

	function createCantinaClickHandler(cantina) {
		return function(event) {
			selectedCantina = cantina;
			$.mobile.changePage('../schedacantina/cantina.html');
		};
	}

}