function CantinaController(cantina) {
	this.cantina = cantina;
	this.view = new CantinaView();
}

CantinaController.prototype.show = function() {
	var controller = this;
	this.view.setCantinaLabel(this.cantina.label);
	this.view.setPalazzoLabel(this.cantina.location.label);
	this.view.setMapClickHandler(function() {
		$.mobile.changePage('../googlemaps/maps.html');
	});

	$.each(this.cantina.etichette, function(index, etichetta) {
		controller.view.addEtichetta(etichetta)
	});
	controller.view.refresh();
};

function CantinaView() {
}

CantinaView.prototype.setMapClickHandler = function(handler) {
	$(document).on("vclick", '#mapsImg', handler);
};

CantinaView.prototype.setCantinaLabel = function(label) {
	$('#cantinaLabel').text(label);
};

CantinaView.prototype.setPalazzoLabel = function(label) {
	$('#palazzoLabel').text(label);
};

CantinaView.prototype.addEtichetta = function(etichetta) {
	var etichettaElement = prototype($('#etichetta'));
	etichettaElement.text(etichetta);
	$('#etichetteListView').append(etichettaElement);
};

CantinaView.prototype.refresh = function() {
	$('#etichetteListView').listview('refresh');
};